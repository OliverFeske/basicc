#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
//#include "file.h"		// searches for a file

#define PI 3.1415			// preprocessing directive, is needed so the variable can be acessed in multiple funtions


struct VideoDetails
{
	int length;
	char uploader[50];
} videoDetail;

struct helloThere {			// the struct has everything set at all times
	int a;
	char b;
}; helloThere;

union helloThereGeneral {			// the union has only ONE member set at a time
	int a;
	char b;
} helloThereGeneral;

int summarized(int num, ...)
{
	va_list valist;
	int sum = 0;

	va_start(valist, num);

	for (int i = 0; i < num; i++)
	{
		sum += va_arg(valist, int);
	}

	va_end(valist);

	return sum;
}

int main()
{
	//// ints
	//int erste = 1;			// ints have a valuerange of -2 147 483 648 to 2 147 483 647
	//unsigned int zweite;	// unsigned ints are only positive
	//zweite = 2;
	//int dritte = erste + zweite;
	//printf("Hello World %d\n", dritte);			// %d for decimal
	//printf("Hello World %u\n", zweite);			// %u for unsigned 

	//// floats / doubles
	//double vierte = 4.2;						// double has the doubled storage capacity and is double as accurate as a float
	//int leet = 1337;
	//double elite = vierte + leet;
	//printf("elite %f\n", elite);			// %f for float 

	//// characters
	//char letter = 'c';
	//printf("The letter is  %c\n", letter);			// %c for characters

	//// constants
	//const float constFloat = 1.1111;
	//printf("constFloat is %f\n", constFloat);
	//printf("PI is %f\n", PI);

	//// if
	//int a = 0;
	//int b = 1;
	//if (b == 0)
	//	printf("a is 0\n");
	//else if (b == 1)
	//	printf("b is 1\n");
	//else
	//	printf("b is not 0 and not 1\n");

	//// operators
	//int c = 5;
	//int d = 4;
	//c++;
	//printf("c is %d\n", c + d);
	//printf("c is %d\n", c - d);
	//printf("c is %d\n", c * d);
	//printf("c is %d\n", c / d);			// dividing ints works as modulo
	//printf("c is %d\n", d % c);			// prints out the rest

	//// switch
	//char grade = 'A';
	//switch (grade)
	//{
	//case 'A':
	//	printf("exellent\n");
	//	break;
	//case 'B':
	//case 'C':
	//	printf("well done\n");
	//	break;
	//case 'D':
	//	printf("needs improvement\n");
	//	break;
	//default:
	//	printf("This isn�t a note\n");
	//	break;
	//}

	//// do while
	//int e = 100;
	//do									// gets executed at least one time
	//{
	//	printf("e is %d\n", e);
	//} while (e < 20);

	//// loop control
	//int loopLength = 10;
	//for (int i = 0; i < loopLength; i++)
	//{
	//	printf("i is %d\n", i);
	//	if (i == 5)
	//	{
	//		printf("break the loop!\n");
	//		//break;							// break the loop
	//		continue;						// is used to skip the current loop state
	//	}
	//}

	//// pointer			points to an adress
	//int f = 5;
	//int* ptr;
	//ptr = &f;
	//printf("%x\n", &f);
	//printf("%d\n", *ptr);			// write a * in front of the pointer to get the actual value and not the adress

	//// pointer on arrays
	//int g[4] = { 10,20,30,40 };
	//int* ptr2;
	//ptr2 = g;
	//printf("%d\n", *(ptr2 + 1));			// +1 prints out the next element

	// strings
	//char myString[] = { 'H', 'e', 'l', 'l', 'o', '\0' };			// \0 at char array to end the array
	//char mySecondString[] = "There";
	//char combination[15];
	//strcpy(combination, myString);			// overrides the value
	//strcat(myString, mySecondString);		// adds something to the first parameter
	//printf("%s\n", combination);

	//// multidimensional arrays
	//int multiArray[3][4] = { {1,2,3,4}, {5,6,7,8}, {9,10,11,12} };
	//for (int i = 0; i < 3; i++)
	//{
	//	for (int j = 0; j < 4; j++)
	//	{
	//		printf("multiArray[%d][%d] = %d\n", i, j, multiArray[i][j]);
	//	}
	//}

	//// structs
	//struct VideoDetails Video1;
	//Video1.length = 10;
	////strcpy(Video1.uploader, "new Uploader");
	//struct VideoDetails Video2;
	//Video2.length = 100;
	////strcpy(Video1.uploader, "another Uploader");
	////printVideoDetails( &Video1 );

	//// unions
	//union helloThereGeneral hTG;
	//hTG.a = 100;
	//hTG.b = 'b';
	//printf("%d\n", hTG.b);

	//// typedef
	//typedef unsigned char BYTE;
	//BYTE byte1 = 50;
	//printf("%d\n", byte1);

	// input and output
	//int h;
	//printf("High Five: ");
	////h = getchar();
	////putchar(h);
	//char str[100];
	//int i;
	////gets(str);
	////puts(str);
	//scanf("%s %d", str, &i);
	//printf("Your Input: %s\n", str, i);

	//// open files , does not work due to deprecation
	//FILE* fileptr;
	//char buffer[255];
	//fileptr = fopen("D:\_ProgrammingProjects\_Basic_C_Home\OpenFileTest.txt", "r");			// Modes: r = read only, w = write only, a = append(write at the end), r+ = read and write, w+ = crunch, a+ = read at front and write at the end
	//char j;
	//j = fgetc(fileptr);
	//while (j != EOF)
	//{
	//	printf("%c", fgetc(fileptr));
	//	j = fgetc(fileptr);
	//} 

	//// va lists 
	//printf("Sum: %d\n", summarized(5, 1, 2, 3, 4, 5));

	// assign memory to the program
	char* description;
	description = (char*)malloc(2000 * sizeof(char));
	if (description == NULL)
	{
		return -1;
	}
	else
	{
		strcpy(description, "This is a description of...");
	}
	printf("%s\n", description);
	description = realloc(description, 100 * sizeof(char));
	free(description);													// GARBAGE COLLECTION

	return 0;
}

void printVideoDetails(struct VideoDetails* vidDet)
{
	printf("length: %d\n", vidDet->length);
	printf("uploader: %s\n", vidDet->uploader);
}